package com.gonet;

import com.gonet.interfaces.MyArrayInterface;

import java.util.ArrayList;

public class MyDynamicArray implements MyArrayInterface {

    private ArrayList<String> myArray;

    public MyDynamicArray(){
        myArray = new ArrayList<String>();
    }

    @Override
    public void convertToDynamicArray(String[] a) {
        for (int i=0;i<a.length;i++){
            myArray.add(a[i]);
            System.out.println("The element: " + a[i]+" has been added at index " + i);
        }
    }

    @Override
    public void clearIndex(int index) {
        System.out.println("The element: " + myArray.get(index)+" has been converted to null ");
        myArray.set(index,"");
    }

    @Override
    public ArrayList<String> obtainArray() {
        ArrayList<String> myArrayClear = new ArrayList<String>();
        for (String s : myArray){
            if (!s.equals("")) myArrayClear.add(s);
        }
        return myArrayClear;
    }
}
