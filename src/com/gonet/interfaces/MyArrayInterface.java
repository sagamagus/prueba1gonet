package com.gonet.interfaces;

import java.util.ArrayList;

public interface MyArrayInterface {

    void convertToDynamicArray(String[] a);
    void clearIndex(int index);
    ArrayList<String> obtainArray();

}
