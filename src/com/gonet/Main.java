package com.gonet;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Scanner keyboard = new Scanner(System.in);
        int myint=0;
        do {
            try {
                System.out.println("Ingresa el tamaño del array estatico");
                myint = keyboard.nextInt();
            } catch (InputMismatchException ime){
                System.out.println("¡Cuidado! Solo puedes insertar números. ");
                keyboard.next();
            }
        } while (myint==0);
        keyboard.nextLine();
        String[] stArray = new String[myint];
        System.out.println("Ingresa elcontenido del array");
        for (int i =0; i<myint;i++){
            System.out.println("Elemento " + i + ":");
            stArray[i] =keyboard.nextLine();
        }
        MyDynamicArray dynamicArray = new MyDynamicArray();
        dynamicArray.convertToDynamicArray(stArray);
        dynamicArray.clearIndex(myint-1);
        ArrayList<String> dinamic =  dynamicArray.obtainArray();
        for (int i =0; i<dinamic.size();i++){
            System.out.println(dinamic.get(i));
        }
    }
}
